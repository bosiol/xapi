package pl.wojciechr.xapi.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.service.InvalidOrderItemException;
import pl.wojciechr.xapi.service.interfaces.IKuponService;
import pl.wojciechr.xapi.service.interfaces.ISysUserService;

@RestController
@RequestMapping("kupon")
public class KuponController {
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private IKuponService kuponService;
	
	
	@GetMapping("getLSU")
	public ResponseEntity<List<KuponUser>> login(@RequestParam(value="sysid", required=true) int sysid,
		    @RequestParam(value="password", required=false) String password,
		    @RequestParam(value="user", required=false) int user,
		    @RequestParam(value="salon", required=false) int salon,
		    @RequestParam(value="card", required=false) int cardId) {
		SysUser sysUser = sysUserService.get(sysid, password);
		List<KuponUser> list = kuponService.getKuponLstUserSalon(user, salon, cardId);
		return new ResponseEntity<List<KuponUser>>(list, HttpStatus.OK);
	}
	
	@GetMapping("getLS")
	public ResponseEntity<List<KuponUser>> login(
			    @RequestParam(value="user", required=false) int user,
		    @RequestParam(value="salon", required=false) int salon
) {
		List<KuponUser> list = kuponService.getKuponLstSalon(user, salon );
		return new ResponseEntity<List<KuponUser>>(list, HttpStatus.OK);
	}
	/*
	@GetMapping("getKR")
	public ResponseEntity<StdResponse> getKr(@RequestParam(value="sysid", required=true) int sysid,
		    @RequestParam(value="password", required=false) String password,
		    @RequestParam(value="user", required=false) int user,
		    @RequestParam(value="salon", required=false) int salon,
		    @RequestParam(value="card", required=false) int cardId) {
		SysUser sysUser = sysUserService.get(sysid, password);
		StdResponse sr = new StdResponse();

		return new ResponseEntity<StdResponse>(sr, HttpStatus.OK);
	}
*/
	@GetMapping("getKR")
	public ResponseEntity<StdResponse> getKuponRabatowy(
			    @RequestParam(value="user", required=false) int user,
		    @RequestParam(value="salon", required=false) int salon,
		    @RequestParam(value="card", required=false) int card,
		    @RequestParam(value="sysid", required=false) int sysid,
		    @RequestParam(value="password", required=false) String password,
		    @RequestParam(value="kupon", required=false) int kupon
) throws InvalidOrderItemException, SQLException {
		StdResponse response = kuponService.getKuponRabatowy(user, salon, card, sysid, kupon, password );
		return new ResponseEntity<StdResponse>(response, HttpStatus.OK);
	}
}
