package pl.wojciechr.xapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.service.interfaces.ICardService;
import pl.wojciechr.xapi.service.interfaces.ISysUserService;

@RestController
@RequestMapping("card")
public class CardController {
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private ICardService cardService;

	// if( isset($_GET["sysid"])&& isset($_GET["password"])&& isset($_GET["user"]) && isset($_GET["salon"]) )
	@GetMapping("get")
	public ResponseEntity<String> login(@RequestParam(value="sysid", required=true) int sysid,
		    @RequestParam(value="password", required=false) String password,
		    @RequestParam(value="user", required=false) int user,
		    @RequestParam(value="salon", required=false) int salon ) throws JsonProcessingException {
		SysUser sysUser = sysUserService.get(sysid, password);
		ObjectMapper mapper = new ObjectMapper();
		
		Card card = null; 
		card = cardService.getByUserSalonSysId(user, salon, sysid);	
		String txt =  mapper.writerWithDefaultPrettyPrinter().writeValueAsString(card);
		return new ResponseEntity<String>(txt, HttpStatus.OK);
	}
	
	@GetMapping("test")
	public ResponseEntity< Object> method() {                                                                                                                                                                                                                                                                                                                                                                                  
	    boolean b = true ;//  logic  here   
	      if (b)  
	        return new ResponseEntity< Object>(HttpStatus.OK);      
	    else      
	        return new ResponseEntity< Object>(HttpStatus.CONFLICT); //appropriate error code   
	}
}
