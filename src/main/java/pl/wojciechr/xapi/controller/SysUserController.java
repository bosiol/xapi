package pl.wojciechr.xapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.service.interfaces.ISysUserService;

@RestController
@RequestMapping("user")
public class SysUserController {
	@Autowired
	private ISysUserService sysUserService;
	
	@GetMapping("getex")
	public ResponseEntity<SysUser> login(@RequestParam(value="email", required=true) String email,
		    @RequestParam(value="password", required=false) String password	 ) {
		SysUser sysUser = sysUserService.get(email, password);
		 
		return new ResponseEntity<SysUser>(sysUser, HttpStatus.OK);
	}
	
	@GetMapping("/getexa")
	public String login2( ) {
	
		 
		return "OK";
	}
	
	@RequestMapping("/metoda") public String metoda() { return "glowny"; }
}
