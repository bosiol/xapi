// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class SysUser implements Serializable {

    /** Primary key. */
    protected static final String PK = "suId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    private Serializable lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Serializable getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Serializable aLockFlag) {
        lockFlag = aLockFlag;
    }

    private int suId;
    private String suEmail;
    private String suPass;
    private short suAnon;
    private String suApass;
    private String suFcmtok;
    private String suHash;
    private String suTel;

    /** Default constructor. */
    public SysUser() {
        super();
    }

    /**
     * Access method for suId.
     *
     * @return the current value of suId
     */
    public int getSuId() {
        return suId;
    }

    /**
     * Setter method for suId.
     *
     * @param aSuId the new value for suId
     */
    public void setSuId(int aSuId) {
        suId = aSuId;
    }

    /**
     * Access method for suEmail.
     *
     * @return the current value of suEmail
     */
    public String getSuEmail() {
        return suEmail;
    }

    /**
     * Setter method for suEmail.
     *
     * @param aSuEmail the new value for suEmail
     */
    public void setSuEmail(String aSuEmail) {
        suEmail = aSuEmail;
    }

    /**
     * Access method for suPass.
     *
     * @return the current value of suPass
     */
    public String getSuPass() {
        return suPass;
    }

    /**
     * Setter method for suPass.
     *
     * @param aSuPass the new value for suPass
     */
    public void setSuPass(String aSuPass) {
        suPass = aSuPass;
    }

    /**
     * Access method for suAnon.
     *
     * @return the current value of suAnon
     */
    public short getSuAnon() {
        return suAnon;
    }

    /**
     * Setter method for suAnon.
     *
     * @param i the new value for suAnon
     */
    public void setSuAnon(int i) {
        suAnon = (short) i;
    }

    /**
     * Access method for suApass.
     *
     * @return the current value of suApass
     */
    public String getSuApass() {
        return suApass;
    }

    /**
     * Setter method for suApass.
     *
     * @param aSuApass the new value for suApass
     */
    public void setSuApass(String aSuApass) {
        suApass = aSuApass;
    }

    /**
     * Access method for suFcmtok.
     *
     * @return the current value of suFcmtok
     */
    public String getSuFcmtok() {
        return suFcmtok;
    }

    /**
     * Setter method for suFcmtok.
     *
     * @param aSuFcmtok the new value for suFcmtok
     */
    public void setSuFcmtok(String aSuFcmtok) {
        suFcmtok = aSuFcmtok;
    }

    /**
     * Access method for suHash.
     *
     * @return the current value of suHash
     */
    public String getSuHash() {
        return suHash;
    }

    /**
     * Setter method for suHash.
     *
     * @param aSuHash the new value for suHash
     */
    public void setSuHash(String aSuHash) {
        suHash = aSuHash;
    }

    /**
     * Access method for suTel.
     *
     * @return the current value of suTel
     */
    public String getSuTel() {
        return suTel;
    }

    /**
     * Setter method for suTel.
     *
     * @param aSuTel the new value for suTel
     */
    public void setSuTel(String aSuTel) {
        suTel = aSuTel;
    }

    /**
     * Compares the key for this instance with another SysUser.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class SysUser and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof SysUser)) {
            return false;
        }
        SysUser that = (SysUser) other;
        if (this.getSuId() != that.getSuId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another SysUser.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof SysUser)) return false;
        return this.equalKeys(other) && ((SysUser)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getSuId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[SysUser |");
        sb.append(" suId=").append(getSuId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("suId", Integer.valueOf(getSuId()));
        return ret;
    }

}
