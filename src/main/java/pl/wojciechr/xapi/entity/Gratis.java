// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public class Gratis implements Serializable {

    /** Primary key. */
    protected static final String PK = "graId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    private Serializable lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Serializable getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Serializable aLockFlag) {
        lockFlag = aLockFlag;
    }

    private int graId;
    private int graBlu;
    private int graUsId;
    private short graSaId;
    private int graZrId;
    private String graNazwa;
    private BigDecimal graProcent;
    private BigDecimal graKwota;
    private int graTowar;
    private int graGrupa;
    private int graData;
    private int graRozdane;
    private int graData1;
    private short graDel;
    private int graWykorzystane;
    private int graLimit;
    private short graWyl;
    private short graAl1;
    private int graFlaga;
    private int graLokal;
    private int graUrlId;
    private int graHtId;
    private int graUgId;

    /** Default constructor. */
    public Gratis() {
        super();
    }

    /**
     * Access method for graId.
     *
     * @return the current value of graId
     */
    public int getGraId() {
        return graId;
    }

    /**
     * Setter method for graId.
     *
     * @param aGraId the new value for graId
     */
    public void setGraId(int aGraId) {
        graId = aGraId;
    }

    /**
     * Access method for graBlu.
     *
     * @return the current value of graBlu
     */
    public int getGraBlu() {
        return graBlu;
    }

    /**
     * Setter method for graBlu.
     *
     * @param aGraBlu the new value for graBlu
     */
    public void setGraBlu(int aGraBlu) {
        graBlu = aGraBlu;
    }

    /**
     * Access method for graUsId.
     *
     * @return the current value of graUsId
     */
    public int getGraUsId() {
        return graUsId;
    }

    /**
     * Setter method for graUsId.
     *
     * @param aGraUsId the new value for graUsId
     */
    public void setGraUsId(int aGraUsId) {
        graUsId = aGraUsId;
    }

    /**
     * Access method for graSaId.
     *
     * @return the current value of graSaId
     */
    public short getGraSaId() {
        return graSaId;
    }

    /**
     * Setter method for graSaId.
     *
     * @param aGraSaId the new value for graSaId
     */
    public void setGraSaId(short aGraSaId) {
        graSaId = aGraSaId;
    }

    /**
     * Access method for graZrId.
     *
     * @return the current value of graZrId
     */
    public int getGraZrId() {
        return graZrId;
    }

    /**
     * Setter method for graZrId.
     *
     * @param aGraZrId the new value for graZrId
     */
    public void setGraZrId(int aGraZrId) {
        graZrId = aGraZrId;
    }

    /**
     * Access method for graNazwa.
     *
     * @return the current value of graNazwa
     */
    public String getGraNazwa() {
        return graNazwa;
    }

    /**
     * Setter method for graNazwa.
     *
     * @param aGraNazwa the new value for graNazwa
     */
    public void setGraNazwa(String aGraNazwa) {
        graNazwa = aGraNazwa;
    }

    /**
     * Access method for graProcent.
     *
     * @return the current value of graProcent
     */
    public BigDecimal getGraProcent() {
        return graProcent;
    }

    /**
     * Setter method for graProcent.
     *
     * @param aGraProcent the new value for graProcent
     */
    public void setGraProcent(BigDecimal aGraProcent) {
        graProcent = aGraProcent;
    }

    /**
     * Access method for graKwota.
     *
     * @return the current value of graKwota
     */
    public BigDecimal getGraKwota() {
        return graKwota;
    }

    /**
     * Setter method for graKwota.
     *
     * @param aGraKwota the new value for graKwota
     */
    public void setGraKwota(BigDecimal aGraKwota) {
        graKwota = aGraKwota;
    }

    /**
     * Access method for graTowar.
     *
     * @return the current value of graTowar
     */
    public int getGraTowar() {
        return graTowar;
    }

    /**
     * Setter method for graTowar.
     *
     * @param aGraTowar the new value for graTowar
     */
    public void setGraTowar(int aGraTowar) {
        graTowar = aGraTowar;
    }

    /**
     * Access method for graGrupa.
     *
     * @return the current value of graGrupa
     */
    public int getGraGrupa() {
        return graGrupa;
    }

    /**
     * Setter method for graGrupa.
     *
     * @param aGraGrupa the new value for graGrupa
     */
    public void setGraGrupa(int aGraGrupa) {
        graGrupa = aGraGrupa;
    }

    /**
     * Access method for graData.
     *
     * @return the current value of graData
     */
    public int getGraData() {
        return graData;
    }

    /**
     * Setter method for graData.
     *
     * @param aGraData the new value for graData
     */
    public void setGraData(int aGraData) {
        graData = aGraData;
    }

    /**
     * Access method for graRozdane.
     *
     * @return the current value of graRozdane
     */
    public int getGraRozdane() {
        return graRozdane;
    }

    /**
     * Setter method for graRozdane.
     *
     * @param aGraRozdane the new value for graRozdane
     */
    public void setGraRozdane(int aGraRozdane) {
        graRozdane = aGraRozdane;
    }

    /**
     * Access method for graData1.
     *
     * @return the current value of graData1
     */
    public int getGraData1() {
        return graData1;
    }

    /**
     * Setter method for graData1.
     *
     * @param aGraData1 the new value for graData1
     */
    public void setGraData1(int aGraData1) {
        graData1 = aGraData1;
    }

    /**
     * Access method for graDel.
     *
     * @return the current value of graDel
     */
    public short getGraDel() {
        return graDel;
    }

    /**
     * Setter method for graDel.
     *
     * @param aGraDel the new value for graDel
     */
    public void setGraDel(short aGraDel) {
        graDel = aGraDel;
    }

    /**
     * Access method for graWykorzystane.
     *
     * @return the current value of graWykorzystane
     */
    public int getGraWykorzystane() {
        return graWykorzystane;
    }

    /**
     * Setter method for graWykorzystane.
     *
     * @param aGraWykorzystane the new value for graWykorzystane
     */
    public void setGraWykorzystane(int aGraWykorzystane) {
        graWykorzystane = aGraWykorzystane;
    }

    /**
     * Access method for graLimit.
     *
     * @return the current value of graLimit
     */
    public int getGraLimit() {
        return graLimit;
    }

    /**
     * Setter method for graLimit.
     *
     * @param aGraLimit the new value for graLimit
     */
    public void setGraLimit(int aGraLimit) {
        graLimit = aGraLimit;
    }

    /**
     * Access method for graWyl.
     *
     * @return the current value of graWyl
     */
    public short getGraWyl() {
        return graWyl;
    }

    /**
     * Setter method for graWyl.
     *
     * @param aGraWyl the new value for graWyl
     */
    public void setGraWyl(short aGraWyl) {
        graWyl = aGraWyl;
    }

    /**
     * Access method for graAl1.
     *
     * @return the current value of graAl1
     */
    public short getGraAl1() {
        return graAl1;
    }

    /**
     * Setter method for graAl1.
     *
     * @param aGraAl1 the new value for graAl1
     */
    public void setGraAl1(short aGraAl1) {
        graAl1 = aGraAl1;
    }

    /**
     * Access method for graFlaga.
     *
     * @return the current value of graFlaga
     */
    public int getGraFlaga() {
        return graFlaga;
    }

    /**
     * Setter method for graFlaga.
     *
     * @param aGraFlaga the new value for graFlaga
     */
    public void setGraFlaga(int aGraFlaga) {
        graFlaga = aGraFlaga;
    }

    /**
     * Access method for graLokal.
     *
     * @return the current value of graLokal
     */
    public int getGraLokal() {
        return graLokal;
    }

    /**
     * Setter method for graLokal.
     *
     * @param aGraLokal the new value for graLokal
     */
    public void setGraLokal(int aGraLokal) {
        graLokal = aGraLokal;
    }

    /**
     * Access method for graUrlId.
     *
     * @return the current value of graUrlId
     */
    public int getGraUrlId() {
        return graUrlId;
    }

    /**
     * Setter method for graUrlId.
     *
     * @param aGraUrlId the new value for graUrlId
     */
    public void setGraUrlId(int aGraUrlId) {
        graUrlId = aGraUrlId;
    }

    /**
     * Access method for graHtId.
     *
     * @return the current value of graHtId
     */
    public int getGraHtId() {
        return graHtId;
    }

    /**
     * Setter method for graHtId.
     *
     * @param aGraHtId the new value for graHtId
     */
    public void setGraHtId(int aGraHtId) {
        graHtId = aGraHtId;
    }

    /**
     * Access method for graUgId.
     *
     * @return the current value of graUgId
     */
    public int getGraUgId() {
        return graUgId;
    }

    /**
     * Setter method for graUgId.
     *
     * @param aGraUgId the new value for graUgId
     */
    public void setGraUgId(int aGraUgId) {
        graUgId = aGraUgId;
    }

    /**
     * Compares the key for this instance with another Gratis.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gratis and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gratis)) {
            return false;
        }
        Gratis that = (Gratis) other;
        if (this.getGraId() != that.getGraId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gratis.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gratis)) return false;
        return this.equalKeys(other) && ((Gratis)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getGraId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gratis |");
        sb.append(" graId=").append(getGraId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("graId", Integer.valueOf(getGraId()));
        return ret;
    }

}
