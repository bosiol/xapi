// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class Salon implements Serializable {

    /** Primary key. */
    protected static final String PK = "saId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    private Serializable lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Serializable getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Serializable aLockFlag) {
        lockFlag = aLockFlag;
    }

    private int saId;
    private int saZrNr;
    private int saUsId;
    private String saOpis;
    private short saAkt;
    private String saPin;
    private int saUrl;
    private String saEmail;
    private String saMobile;
    private String saTimes;
    private String saLocal;
    private int saAdId;
    private int saUpdate;
    private String saParam;
    private String saGuid;
    private short saSet;
    private int saFlags;

    /** Default constructor. */
    public Salon() {
        super();
    }

    /**
     * Access method for saId.
     *
     * @return the current value of saId
     */
    public int getSaId() {
        return saId;
    }

    /**
     * Setter method for saId.
     *
     * @param aSaId the new value for saId
     */
    public void setSaId(int aSaId) {
        saId = aSaId;
    }

    /**
     * Access method for saZrNr.
     *
     * @return the current value of saZrNr
     */
    public int getSaZrNr() {
        return saZrNr;
    }

    /**
     * Setter method for saZrNr.
     *
     * @param aSaZrNr the new value for saZrNr
     */
    public void setSaZrNr(int aSaZrNr) {
        saZrNr = aSaZrNr;
    }

    /**
     * Access method for saUsId.
     *
     * @return the current value of saUsId
     */
    public int getSaUsId() {
        return saUsId;
    }

    /**
     * Setter method for saUsId.
     *
     * @param aSaUsId the new value for saUsId
     */
    public void setSaUsId(int aSaUsId) {
        saUsId = aSaUsId;
    }

    /**
     * Access method for saOpis.
     *
     * @return the current value of saOpis
     */
    public String getSaOpis() {
        return saOpis;
    }

    /**
     * Setter method for saOpis.
     *
     * @param aSaOpis the new value for saOpis
     */
    public void setSaOpis(String aSaOpis) {
        saOpis = aSaOpis;
    }

    /**
     * Access method for saAkt.
     *
     * @return the current value of saAkt
     */
    public short getSaAkt() {
        return saAkt;
    }

    /**
     * Setter method for saAkt.
     *
     * @param aSaAkt the new value for saAkt
     */
    public void setSaAkt(short aSaAkt) {
        saAkt = aSaAkt;
    }

    /**
     * Access method for saPin.
     *
     * @return the current value of saPin
     */
    public String getSaPin() {
        return saPin;
    }

    /**
     * Setter method for saPin.
     *
     * @param aSaPin the new value for saPin
     */
    public void setSaPin(String aSaPin) {
        saPin = aSaPin;
    }

    /**
     * Access method for saUrl.
     *
     * @return the current value of saUrl
     */
    public int getSaUrl() {
        return saUrl;
    }

    /**
     * Setter method for saUrl.
     *
     * @param aSaUrl the new value for saUrl
     */
    public void setSaUrl(int aSaUrl) {
        saUrl = aSaUrl;
    }

    /**
     * Access method for saEmail.
     *
     * @return the current value of saEmail
     */
    public String getSaEmail() {
        return saEmail;
    }

    /**
     * Setter method for saEmail.
     *
     * @param aSaEmail the new value for saEmail
     */
    public void setSaEmail(String aSaEmail) {
        saEmail = aSaEmail;
    }

    /**
     * Access method for saMobile.
     *
     * @return the current value of saMobile
     */
    public String getSaMobile() {
        return saMobile;
    }

    /**
     * Setter method for saMobile.
     *
     * @param aSaMobile the new value for saMobile
     */
    public void setSaMobile(String aSaMobile) {
        saMobile = aSaMobile;
    }

    /**
     * Access method for saTimes.
     *
     * @return the current value of saTimes
     */
    public String getSaTimes() {
        return saTimes;
    }

    /**
     * Setter method for saTimes.
     *
     * @param aSaTimes the new value for saTimes
     */
    public void setSaTimes(String aSaTimes) {
        saTimes = aSaTimes;
    }

    /**
     * Access method for saLocal.
     *
     * @return the current value of saLocal
     */
    public String getSaLocal() {
        return saLocal;
    }

    /**
     * Setter method for saLocal.
     *
     * @param aSaLocal the new value for saLocal
     */
    public void setSaLocal(String aSaLocal) {
        saLocal = aSaLocal;
    }

    /**
     * Access method for saAdId.
     *
     * @return the current value of saAdId
     */
    public int getSaAdId() {
        return saAdId;
    }

    /**
     * Setter method for saAdId.
     *
     * @param aSaAdId the new value for saAdId
     */
    public void setSaAdId(int aSaAdId) {
        saAdId = aSaAdId;
    }

    /**
     * Access method for saUpdate.
     *
     * @return the current value of saUpdate
     */
    public int getSaUpdate() {
        return saUpdate;
    }

    /**
     * Setter method for saUpdate.
     *
     * @param aSaUpdate the new value for saUpdate
     */
    public void setSaUpdate(int aSaUpdate) {
        saUpdate = aSaUpdate;
    }

    /**
     * Access method for saParam.
     *
     * @return the current value of saParam
     */
    public String getSaParam() {
        return saParam;
    }

    /**
     * Setter method for saParam.
     *
     * @param aSaParam the new value for saParam
     */
    public void setSaParam(String aSaParam) {
        saParam = aSaParam;
    }

    /**
     * Access method for saGuid.
     *
     * @return the current value of saGuid
     */
    public String getSaGuid() {
        return saGuid;
    }

    /**
     * Setter method for saGuid.
     *
     * @param aSaGuid the new value for saGuid
     */
    public void setSaGuid(String aSaGuid) {
        saGuid = aSaGuid;
    }

    /**
     * Access method for saSet.
     *
     * @return the current value of saSet
     */
    public short getSaSet() {
        return saSet;
    }

    /**
     * Setter method for saSet.
     *
     * @param aSaSet the new value for saSet
     */
    public void setSaSet(short aSaSet) {
        saSet = aSaSet;
    }

    /**
     * Access method for saFlags.
     *
     * @return the current value of saFlags
     */
    public int getSaFlags() {
        return saFlags;
    }

    /**
     * Setter method for saFlags.
     *
     * @param aSaFlags the new value for saFlags
     */
    public void setSaFlags(int aSaFlags) {
        saFlags = aSaFlags;
    }

    /**
     * Compares the key for this instance with another Salon.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Salon and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Salon)) {
            return false;
        }
        Salon that = (Salon) other;
        if (this.getSaId() != that.getSaId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Salon.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Salon)) return false;
        return this.equalKeys(other) && ((Salon)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getSaId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Salon |");
        sb.append(" saId=").append(getSaId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("saId", Integer.valueOf(getSaId()));
        return ret;
    }

}
