package pl.wojciechr.xapi.entity;

import java.io.Serializable;

public class StdResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int status;
	private String desc;
	
	public StdResponse() {
		super();
	}
	public StdResponse(int id, int status, String desc) {
		super();
		this.id = id;
		this.status = status;
		this.desc = desc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[StdResponse |");
        sb.append(" id=").append(getId());
        sb.append(" status=").append(getStatus());
        sb.append(" desc=").append(getDesc());
        sb.append("]");
        return sb.toString();
    }
}
