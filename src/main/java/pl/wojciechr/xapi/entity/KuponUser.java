// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class KuponUser implements Serializable {


    
    public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNazwa() {
		return Nazwa;
	}

	public void setNazwa(String nazwa) {
		Nazwa = nazwa;
	}

	public int getIle() {
		return Ile;
	}

	public void setIle(int ile) {
		Ile = ile;
	}

	public int getPobrano() {
		return Pobrano;
	}

	public void setPobrano(int pobrano) {
		Pobrano = pobrano;
	}

	public String getDataW() {
		return DataW;
	}

	public void setDataW(String dataW) {
		DataW = dataW;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getOpis() {
		return Opis;
	}

	public void setOpis(String opis) {
		Opis = opis;
	}



	private int Id;
	private int KuUsId;
	private int KuSaId;
	private String KuKod;
	private int KuKaZrId;
	private int KuKrZrId;
	private int KuLimit;
	private int KuAkt;
    private String Nazwa;
    private int Ile;
    private int Pobrano;
    private String DataW;
    private String URL;
    private String Opis;

    

    public int getKuUsId() {
		return KuUsId;
	}

	public void setKuUsId(int kuUsId) {
		KuUsId = kuUsId;
	}

	public int getKuSaId() {
		return KuSaId;
	}

	public void setKuSaId(int kuSaId) {
		KuSaId = kuSaId;
	}

	public String getKuKod() {
		return KuKod;
	}

	public void setKuKod(String kuKod) {
		KuKod = kuKod;
	}

	public int getKuKaZrId() {
		return KuKaZrId;
	}

	public void setKuKaZrId(int kuKaZrId) {
		KuKaZrId = kuKaZrId;
	}

	public int getKuKrZrId() {
		return KuKrZrId;
	}

	public void setKuKrZrId(int kuKrZrId) {
		KuKrZrId = kuKrZrId;
	}

	public int getKuLimit() {
		return KuLimit;
	}

	public void setKuLimit(int kuLimit) {
		KuLimit = kuLimit;
	}

	public int getKuAkt() {
		return KuAkt;
	}

	public void setKuAkt(int kuAkt) {
		KuAkt = kuAkt;
	}

	/** Default constructor. */
    public KuponUser() {
        super();
    }




    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Kupon |");
        sb.append(" Id=").append(getId());
        sb.append("]");
        return sb.toString();
    }
    
    public String toJson()
    {
		 StringBuffer sb = new StringBuffer(" { ");
		sb.append(" \"Id\": \"").append(getId()).append("\"");
		 sb.append(" \"Nazwa\": \"").append(getNazwa()).append("\"");
		 sb.append(" \"Ile\": \"").append(getIle()).append("\"");
		 sb.append(" \"Pobrano\": \"").append(getPobrano()).append("\"");
		 sb.append(" \"DataW\": \"").append(getDataW()).append("\"");
		 sb.append(" \"URL\": \"").append(getURL()).append("\"");
		 sb.append(" \"Opis\": \"").append(getOpis()).append("\"");
         sb.append("}");
         return sb.toString();
    }
    /*
     * 
     public String toString() {
		return " { \"Wiek\" : \"" + Wiek + "\", \"Imie\" : \"" + Imie + "\", \"Nazwisko\""
				+ " : \"" + Nazwisko 
				+ "\"}";
	}
     */



}
