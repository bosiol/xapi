// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

public class Card implements Serializable {

    /** Primary key. */
    protected static final String PK = "kaId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    private Serializable lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Serializable getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Serializable aLockFlag) {
        lockFlag = aLockFlag;
    }

    private int kaId;
    private int kaBlu;
    private int kaUsId;
    private short kaAkt;
    private int kaDataAkv;
    private String kaKod;
    private int kaUpustId;
    private int kaDotyczy;
    private int kaWaznosc;
    private short kaLokal;
    private short kaTypklienta;
    private int kaKlientid;
    private short kaTmp;
    private short kaMlmuse;
    private short kaAssign;
    private int kaTwId;
    private int kaRez1;
    private short kaOnline;
    private int kaUpdate;
    private int kaFlaga;
    private int kaPack;
    private int kaUserSys;
    private BigDecimal kaKwotaUp;
    private BigDecimal kaWlimit;
    private int kaPunkty;
    private short kaL1;
    private short kaL2;
    private short kaL3;
    private int kaPunktym;
    private int kaPunktymlm;
    private int kaMinuty;

    /** Default constructor. */
    public Card() {
        super();
    }

    /**
     * Access method for kaId.
     *
     * @return the current value of kaId
     */
    public int getKaId() {
        return kaId;
    }

    /**
     * Setter method for kaId.
     *
     * @param aKaId the new value for kaId
     */
    public void setKaId(int aKaId) {
        kaId = aKaId;
    }

    /**
     * Access method for kaBlu.
     *
     * @return the current value of kaBlu
     */
    public int getKaBlu() {
        return kaBlu;
    }

    /**
     * Setter method for kaBlu.
     *
     * @param aKaBlu the new value for kaBlu
     */
    public void setKaBlu(int aKaBlu) {
        kaBlu = aKaBlu;
    }

    /**
     * Access method for kaUsId.
     *
     * @return the current value of kaUsId
     */
    public int getKaUsId() {
        return kaUsId;
    }

    /**
     * Setter method for kaUsId.
     *
     * @param aKaUsId the new value for kaUsId
     */
    public void setKaUsId(int aKaUsId) {
        kaUsId = aKaUsId;
    }

    /**
     * Access method for kaAkt.
     *
     * @return the current value of kaAkt
     */
    public short getKaAkt() {
        return kaAkt;
    }

    /**
     * Setter method for kaAkt.
     *
     * @param aKaAkt the new value for kaAkt
     */
    public void setKaAkt(short aKaAkt) {
        kaAkt = aKaAkt;
    }

    /**
     * Access method for kaDataAkv.
     *
     * @return the current value of kaDataAkv
     */
    public int getKaDataAkv() {
        return kaDataAkv;
    }

    /**
     * Setter method for kaDataAkv.
     *
     * @param aKaDataAkv the new value for kaDataAkv
     */
    public void setKaDataAkv(int aKaDataAkv) {
        kaDataAkv = aKaDataAkv;
    }

    /**
     * Access method for kaKod.
     *
     * @return the current value of kaKod
     */
    public String getKaKod() {
        return kaKod;
    }

    /**
     * Setter method for kaKod.
     *
     * @param aKaKod the new value for kaKod
     */
    public void setKaKod(String aKaKod) {
        kaKod = aKaKod;
    }

    /**
     * Access method for kaUpustId.
     *
     * @return the current value of kaUpustId
     */
    public int getKaUpustId() {
        return kaUpustId;
    }

    /**
     * Setter method for kaUpustId.
     *
     * @param aKaUpustId the new value for kaUpustId
     */
    public void setKaUpustId(int aKaUpustId) {
        kaUpustId = aKaUpustId;
    }

    /**
     * Access method for kaDotyczy.
     *
     * @return the current value of kaDotyczy
     */
    public int getKaDotyczy() {
        return kaDotyczy;
    }

    /**
     * Setter method for kaDotyczy.
     *
     * @param aKaDotyczy the new value for kaDotyczy
     */
    public void setKaDotyczy(int aKaDotyczy) {
        kaDotyczy = aKaDotyczy;
    }

    /**
     * Access method for kaWaznosc.
     *
     * @return the current value of kaWaznosc
     */
    public int getKaWaznosc() {
        return kaWaznosc;
    }

    /**
     * Setter method for kaWaznosc.
     *
     * @param aKaWaznosc the new value for kaWaznosc
     */
    public void setKaWaznosc(int aKaWaznosc) {
        kaWaznosc = aKaWaznosc;
    }

    /**
     * Access method for kaLokal.
     *
     * @return the current value of kaLokal
     */
    public short getKaLokal() {
        return kaLokal;
    }

    /**
     * Setter method for kaLokal.
     *
     * @param aKaLokal the new value for kaLokal
     */
    public void setKaLokal(short aKaLokal) {
        kaLokal = aKaLokal;
    }

    /**
     * Access method for kaTypklienta.
     *
     * @return the current value of kaTypklienta
     */
    public short getKaTypklienta() {
        return kaTypklienta;
    }

    /**
     * Setter method for kaTypklienta.
     *
     * @param aKaTypklienta the new value for kaTypklienta
     */
    public void setKaTypklienta(short aKaTypklienta) {
        kaTypklienta = aKaTypklienta;
    }

    /**
     * Access method for kaKlientid.
     *
     * @return the current value of kaKlientid
     */
    public int getKaKlientid() {
        return kaKlientid;
    }

    /**
     * Setter method for kaKlientid.
     *
     * @param aKaKlientid the new value for kaKlientid
     */
    public void setKaKlientid(int aKaKlientid) {
        kaKlientid = aKaKlientid;
    }

    /**
     * Access method for kaTmp.
     *
     * @return the current value of kaTmp
     */
    public short getKaTmp() {
        return kaTmp;
    }

    /**
     * Setter method for kaTmp.
     *
     * @param aKaTmp the new value for kaTmp
     */
    public void setKaTmp(short aKaTmp) {
        kaTmp = aKaTmp;
    }

    /**
     * Access method for kaMlmuse.
     *
     * @return the current value of kaMlmuse
     */
    public short getKaMlmuse() {
        return kaMlmuse;
    }

    /**
     * Setter method for kaMlmuse.
     *
     * @param aKaMlmuse the new value for kaMlmuse
     */
    public void setKaMlmuse(short aKaMlmuse) {
        kaMlmuse = aKaMlmuse;
    }

    /**
     * Access method for kaAssign.
     *
     * @return the current value of kaAssign
     */
    public short getKaAssign() {
        return kaAssign;
    }

    /**
     * Setter method for kaAssign.
     *
     * @param aKaAssign the new value for kaAssign
     */
    public void setKaAssign(short aKaAssign) {
        kaAssign = aKaAssign;
    }

    /**
     * Access method for kaTwId.
     *
     * @return the current value of kaTwId
     */
    public int getKaTwId() {
        return kaTwId;
    }

    /**
     * Setter method for kaTwId.
     *
     * @param aKaTwId the new value for kaTwId
     */
    public void setKaTwId(int aKaTwId) {
        kaTwId = aKaTwId;
    }

    /**
     * Access method for kaRez1.
     *
     * @return the current value of kaRez1
     */
    public int getKaRez1() {
        return kaRez1;
    }

    /**
     * Setter method for kaRez1.
     *
     * @param aKaRez1 the new value for kaRez1
     */
    public void setKaRez1(int aKaRez1) {
        kaRez1 = aKaRez1;
    }

    /**
     * Access method for kaOnline.
     *
     * @return the current value of kaOnline
     */
    public short getKaOnline() {
        return kaOnline;
    }

    /**
     * Setter method for kaOnline.
     *
     * @param aKaOnline the new value for kaOnline
     */
    public void setKaOnline(short aKaOnline) {
        kaOnline = aKaOnline;
    }

    /**
     * Access method for kaUpdate.
     *
     * @return the current value of kaUpdate
     */
    public int getKaUpdate() {
        return kaUpdate;
    }

    /**
     * Setter method for kaUpdate.
     *
     * @param aKaUpdate the new value for kaUpdate
     */
    public void setKaUpdate(int aKaUpdate) {
        kaUpdate = aKaUpdate;
    }

    /**
     * Access method for kaFlaga.
     *
     * @return the current value of kaFlaga
     */
    public int getKaFlaga() {
        return kaFlaga;
    }

    /**
     * Setter method for kaFlaga.
     *
     * @param aKaFlaga the new value for kaFlaga
     */
    public void setKaFlaga(int aKaFlaga) {
        kaFlaga = aKaFlaga;
    }

    /**
     * Access method for kaPack.
     *
     * @return the current value of kaPack
     */
    public int getKaPack() {
        return kaPack;
    }

    /**
     * Setter method for kaPack.
     *
     * @param aKaPack the new value for kaPack
     */
    public void setKaPack(int aKaPack) {
        kaPack = aKaPack;
    }

    /**
     * Access method for kaUserSys.
     *
     * @return the current value of kaUserSys
     */
    public int getKaUserSys() {
        return kaUserSys;
    }

    /**
     * Setter method for kaUserSys.
     *
     * @param aKaUserSys the new value for kaUserSys
     */
    public void setKaUserSys(int aKaUserSys) {
        kaUserSys = aKaUserSys;
    }

    /**
     * Access method for kaKwotaUp.
     *
     * @return the current value of kaKwotaUp
     */
    public BigDecimal getKaKwotaUp() {
        return kaKwotaUp;
    }

    /**
     * Setter method for kaKwotaUp.
     *
     * @param aKaKwotaUp the new value for kaKwotaUp
     */
    public void setKaKwotaUp(BigDecimal aKaKwotaUp) {
        kaKwotaUp = aKaKwotaUp;
    }

    /**
     * Access method for kaWlimit.
     *
     * @return the current value of kaWlimit
     */
    public BigDecimal getKaWlimit() {
        return kaWlimit;
    }

    /**
     * Setter method for kaWlimit.
     *
     * @param aKaWlimit the new value for kaWlimit
     */
    public void setKaWlimit(BigDecimal aKaWlimit) {
        kaWlimit = aKaWlimit;
    }

    /**
     * Access method for kaPunkty.
     *
     * @return the current value of kaPunkty
     */
    public int getKaPunkty() {
        return kaPunkty;
    }

    /**
     * Setter method for kaPunkty.
     *
     * @param aKaPunkty the new value for kaPunkty
     */
    public void setKaPunkty(int aKaPunkty) {
        kaPunkty = aKaPunkty;
    }

    /**
     * Access method for kaL1.
     *
     * @return the current value of kaL1
     */
    public short getKaL1() {
        return kaL1;
    }

    /**
     * Setter method for kaL1.
     *
     * @param aKaL1 the new value for kaL1
     */
    public void setKaL1(short aKaL1) {
        kaL1 = aKaL1;
    }

    /**
     * Access method for kaL2.
     *
     * @return the current value of kaL2
     */
    public short getKaL2() {
        return kaL2;
    }

    /**
     * Setter method for kaL2.
     *
     * @param aKaL2 the new value for kaL2
     */
    public void setKaL2(short aKaL2) {
        kaL2 = aKaL2;
    }

    /**
     * Access method for kaL3.
     *
     * @return the current value of kaL3
     */
    public short getKaL3() {
        return kaL3;
    }

    /**
     * Setter method for kaL3.
     *
     * @param aKaL3 the new value for kaL3
     */
    public void setKaL3(short aKaL3) {
        kaL3 = aKaL3;
    }

    /**
     * Access method for kaPunktym.
     *
     * @return the current value of kaPunktym
     */
    public int getKaPunktym() {
        return kaPunktym;
    }

    /**
     * Setter method for kaPunktym.
     *
     * @param aKaPunktym the new value for kaPunktym
     */
    public void setKaPunktym(int aKaPunktym) {
        kaPunktym = aKaPunktym;
    }

    /**
     * Access method for kaPunktymlm.
     *
     * @return the current value of kaPunktymlm
     */
    public int getKaPunktymlm() {
        return kaPunktymlm;
    }

    /**
     * Setter method for kaPunktymlm.
     *
     * @param aKaPunktymlm the new value for kaPunktymlm
     */
    public void setKaPunktymlm(int aKaPunktymlm) {
        kaPunktymlm = aKaPunktymlm;
    }

    /**
     * Access method for kaMinuty.
     *
     * @return the current value of kaMinuty
     */
    public int getKaMinuty() {
        return kaMinuty;
    }

    /**
     * Setter method for kaMinuty.
     *
     * @param aKaMinuty the new value for kaMinuty
     */
    public void setKaMinuty(int aKaMinuty) {
        kaMinuty = aKaMinuty;
    }

    /**
     * Compares the key for this instance with another Card.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Card and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Card)) {
            return false;
        }
        Card that = (Card) other;
        if (this.getKaId() != that.getKaId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Card.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Card)) return false;
        return this.equalKeys(other) && ((Card)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getKaId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Card |");
        sb.append(" kaId=").append(getKaId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("kaId", Integer.valueOf(getKaId()));
        return ret;
    }

}
