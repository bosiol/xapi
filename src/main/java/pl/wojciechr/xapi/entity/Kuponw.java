// Generated with g9.

package pl.wojciechr.xapi.entity;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class Kuponw implements Serializable {

    /** Primary key. */
    protected static final String PK = "krId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    private Serializable lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Serializable getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Serializable aLockFlag) {
        lockFlag = aLockFlag;
    }

    private int krId;
    private int krUsId;
    private int krSaId;
    private int krZrId;
    private String krOpis;
    private String krKod;
    private int krLimit;
    private int krOpZrId;
    private int krOhZrId;
    private int krMsgId;
    private int krLiId;
    private int krFlaga;
    private int krDataUtw;
    private int krDataAkt;
    private int krDataWaz;
    private short krPush;
    private short krAkt;
    private int krUpd;
    private int krUsed;
    private int krUgId;

    /** Default constructor. */
    public Kuponw() {
        super();
    }

    /**
     * Access method for krId.
     *
     * @return the current value of krId
     */
    public int getKrId() {
        return krId;
    }

    /**
     * Setter method for krId.
     *
     * @param aKrId the new value for krId
     */
    public void setKrId(int aKrId) {
        krId = aKrId;
    }

    /**
     * Access method for krUsId.
     *
     * @return the current value of krUsId
     */
    public int getKrUsId() {
        return krUsId;
    }

    /**
     * Setter method for krUsId.
     *
     * @param aKrUsId the new value for krUsId
     */
    public void setKrUsId(int aKrUsId) {
        krUsId = aKrUsId;
    }

    /**
     * Access method for krSaId.
     *
     * @return the current value of krSaId
     */
    public int getKrSaId() {
        return krSaId;
    }

    /**
     * Setter method for krSaId.
     *
     * @param aKrSaId the new value for krSaId
     */
    public void setKrSaId(int aKrSaId) {
        krSaId = aKrSaId;
    }

    /**
     * Access method for krZrId.
     *
     * @return the current value of krZrId
     */
    public int getKrZrId() {
        return krZrId;
    }

    /**
     * Setter method for krZrId.
     *
     * @param aKrZrId the new value for krZrId
     */
    public void setKrZrId(int aKrZrId) {
        krZrId = aKrZrId;
    }

    /**
     * Access method for krOpis.
     *
     * @return the current value of krOpis
     */
    public String getKrOpis() {
        return krOpis;
    }

    /**
     * Setter method for krOpis.
     *
     * @param aKrOpis the new value for krOpis
     */
    public void setKrOpis(String aKrOpis) {
        krOpis = aKrOpis;
    }

    /**
     * Access method for krKod.
     *
     * @return the current value of krKod
     */
    public String getKrKod() {
        return krKod;
    }

    /**
     * Setter method for krKod.
     *
     * @param aKrKod the new value for krKod
     */
    public void setKrKod(String aKrKod) {
        krKod = aKrKod;
    }

    /**
     * Access method for krLimit.
     *
     * @return the current value of krLimit
     */
    public int getKrLimit() {
        return krLimit;
    }

    /**
     * Setter method for krLimit.
     *
     * @param aKrLimit the new value for krLimit
     */
    public void setKrLimit(int aKrLimit) {
        krLimit = aKrLimit;
    }

    /**
     * Access method for krOpZrId.
     *
     * @return the current value of krOpZrId
     */
    public int getKrOpZrId() {
        return krOpZrId;
    }

    /**
     * Setter method for krOpZrId.
     *
     * @param aKrOpZrId the new value for krOpZrId
     */
    public void setKrOpZrId(int aKrOpZrId) {
        krOpZrId = aKrOpZrId;
    }

    /**
     * Access method for krOhZrId.
     *
     * @return the current value of krOhZrId
     */
    public int getKrOhZrId() {
        return krOhZrId;
    }

    /**
     * Setter method for krOhZrId.
     *
     * @param aKrOhZrId the new value for krOhZrId
     */
    public void setKrOhZrId(int aKrOhZrId) {
        krOhZrId = aKrOhZrId;
    }

    /**
     * Access method for krMsgId.
     *
     * @return the current value of krMsgId
     */
    public int getKrMsgId() {
        return krMsgId;
    }

    /**
     * Setter method for krMsgId.
     *
     * @param aKrMsgId the new value for krMsgId
     */
    public void setKrMsgId(int aKrMsgId) {
        krMsgId = aKrMsgId;
    }

    /**
     * Access method for krLiId.
     *
     * @return the current value of krLiId
     */
    public int getKrLiId() {
        return krLiId;
    }

    /**
     * Setter method for krLiId.
     *
     * @param aKrLiId the new value for krLiId
     */
    public void setKrLiId(int aKrLiId) {
        krLiId = aKrLiId;
    }

    /**
     * Access method for krFlaga.
     *
     * @return the current value of krFlaga
     */
    public int getKrFlaga() {
        return krFlaga;
    }

    /**
     * Setter method for krFlaga.
     *
     * @param aKrFlaga the new value for krFlaga
     */
    public void setKrFlaga(int aKrFlaga) {
        krFlaga = aKrFlaga;
    }

    /**
     * Access method for krDataUtw.
     *
     * @return the current value of krDataUtw
     */
    public int getKrDataUtw() {
        return krDataUtw;
    }

    /**
     * Setter method for krDataUtw.
     *
     * @param aKrDataUtw the new value for krDataUtw
     */
    public void setKrDataUtw(int aKrDataUtw) {
        krDataUtw = aKrDataUtw;
    }

    /**
     * Access method for krDataAkt.
     *
     * @return the current value of krDataAkt
     */
    public int getKrDataAkt() {
        return krDataAkt;
    }

    /**
     * Setter method for krDataAkt.
     *
     * @param aKrDataAkt the new value for krDataAkt
     */
    public void setKrDataAkt(int aKrDataAkt) {
        krDataAkt = aKrDataAkt;
    }

    /**
     * Access method for krDataWaz.
     *
     * @return the current value of krDataWaz
     */
    public int getKrDataWaz() {
        return krDataWaz;
    }

    /**
     * Setter method for krDataWaz.
     *
     * @param aKrDataWaz the new value for krDataWaz
     */
    public void setKrDataWaz(int aKrDataWaz) {
        krDataWaz = aKrDataWaz;
    }

    /**
     * Access method for krPush.
     *
     * @return the current value of krPush
     */
    public short getKrPush() {
        return krPush;
    }

    /**
     * Setter method for krPush.
     *
     * @param aKrPush the new value for krPush
     */
    public void setKrPush(short aKrPush) {
        krPush = aKrPush;
    }

    /**
     * Access method for krAkt.
     *
     * @return the current value of krAkt
     */
    public short getKrAkt() {
        return krAkt;
    }

    /**
     * Setter method for krAkt.
     *
     * @param aKrAkt the new value for krAkt
     */
    public void setKrAkt(short aKrAkt) {
        krAkt = aKrAkt;
    }

    /**
     * Access method for krUpd.
     *
     * @return the current value of krUpd
     */
    public int getKrUpd() {
        return krUpd;
    }

    /**
     * Setter method for krUpd.
     *
     * @param aKrUpd the new value for krUpd
     */
    public void setKrUpd(int aKrUpd) {
        krUpd = aKrUpd;
    }

    /**
     * Access method for krUsed.
     *
     * @return the current value of krUsed
     */
    public int getKrUsed() {
        return krUsed;
    }

    /**
     * Setter method for krUsed.
     *
     * @param aKrUsed the new value for krUsed
     */
    public void setKrUsed(int aKrUsed) {
        krUsed = aKrUsed;
    }

    /**
     * Access method for krUgId.
     *
     * @return the current value of krUgId
     */
    public int getKrUgId() {
        return krUgId;
    }

    /**
     * Setter method for krUgId.
     *
     * @param aKrUgId the new value for krUgId
     */
    public void setKrUgId(int aKrUgId) {
        krUgId = aKrUgId;
    }

    /**
     * Compares the key for this instance with another Kuponw.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Kuponw and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Kuponw)) {
            return false;
        }
        Kuponw that = (Kuponw) other;
        if (this.getKrId() != that.getKrId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Kuponw.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Kuponw)) return false;
        return this.equalKeys(other) && ((Kuponw)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getKrId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Kuponw |");
        sb.append(" krId=").append(getKrId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("krId", Integer.valueOf(getKrId()));
        return ret;
    }

}
