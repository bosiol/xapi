package pl.wojciechr.xapi.entity.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pl.wojciechr.xapi.entity.Card;

public class CardRowMapper  implements RowMapper<Card> {

	@Override
	public Card mapRow(ResultSet rs, int rowNum) throws SQLException {
		Card card = new Card();
		card.setKaId(rs.getInt("KA_ID"));
		card.setKaKod(rs.getString("KA_KOD"));
		card.setKaPunkty(rs.getInt("KA_PUNKTY"));
		return card;
	}

}
