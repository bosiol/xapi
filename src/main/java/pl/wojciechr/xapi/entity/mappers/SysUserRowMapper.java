package pl.wojciechr.xapi.entity.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.SysUser;

public class SysUserRowMapper implements RowMapper<SysUser> {

	@Override
	public SysUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		SysUser sysUser = new SysUser();
		sysUser.setSuId(rs.getInt("SU_ID"));
		sysUser.setSuEmail(rs.getString("SU_EMAIL"));
		sysUser.setSuPass(rs.getString("SU_PASS"));
		sysUser.setSuAnon(rs.getInt("SU_ANON"));
		sysUser.setSuApass(rs.getString("SU_APASS"));
		sysUser.setSuFcmtok(rs.getString("SU_FCMTOK"));
		sysUser.setSuHash(rs.getString("SU_HASH"));
		sysUser.setSuTel(rs.getString("SU_TEL"));
		return sysUser;

	}

}
