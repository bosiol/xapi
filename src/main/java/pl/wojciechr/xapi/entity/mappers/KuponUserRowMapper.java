package pl.wojciechr.xapi.entity.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.entity.KuponUser;

public class KuponUserRowMapper implements RowMapper<KuponUser> {

	@Override
	public KuponUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		KuponUser kupon = new KuponUser();
		if(rowNum ==0)
			return null;	
		if( !rs.isBeforeFirst())
			return null;
		kupon.setId(rs.getInt("ID"));
	    kupon.setNazwa(rs.getString("NAZWA"));
	    kupon.setIle(rs.getInt("ILE"));
	    kupon.setPobrano(rs.getInt("POBRANO"));
	    kupon.setDataW(rs.getString("DATAW"));
	    kupon.setURL(rs.getString("URL"));
	    kupon.setOpis(rs.getString("OPIS"));
		return kupon;
	 
	}

}
