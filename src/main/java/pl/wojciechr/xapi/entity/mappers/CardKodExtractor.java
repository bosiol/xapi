package pl.wojciechr.xapi.entity.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class CardKodExtractor implements ResultSetExtractor<String> {

	@Override
	public String extractData(ResultSet rs) throws SQLException, DataAccessException {
		String ret ="";	
		while(rs.next())
		{
			ret = rs.getString("KA_KOD");
		}
		return ret;
	}

}
