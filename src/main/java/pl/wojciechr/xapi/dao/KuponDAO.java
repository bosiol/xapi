package pl.wojciechr.xapi.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.mappers.CardRowMapper;
import pl.wojciechr.xapi.entity.mappers.KuponUserRowMapper;

	
@Repository
public class KuponDAO implements IKuponDAO {

	private static final String  SQL_GET_KUPON_LST_SALON_USER =  " select ku.KU_ID AS ID, kw.KR_OPIS AS NAZWA,  kw.KR_LIMIT AS ILE, kw.KR_USED as POBRANO, "
			+ " ku.KU_DATA_WAZ as DATAW ,COALESCE(LI_URL,' ') as URL, oh.OH_TXT as OPIS  from kupon ku  "
			+ " join kuponw kw on ku.KU_US_ID = kw.KR_US_ID and ku.KU_SA_ID = kw.KR_SA_ID and ku.KU_KR_ZR_ID = kw.KR_ZR_ID  "
	        + " left join link li on li.LI_US_ID = kw.KR_US_ID and li.LI_SA_ID = kw.KR_SA_ID and li.LI_ZR_ID = kw.KR_LI_ID "
			+ " left join opish oh on oh.OH_US_ID = kw.KR_US_ID and oh.OH_SA_ID = kw.KR_SA_ID "
	        + " and oh.OH_ZR_ID = kw.KR_OH_ZR_ID "
	        + " where ku.KU_US_ID = ? and ku.KU_SA_ID = ? AND ku.KU_KA_ZR_ID = ?  and ku.KU_AKT = 1 ORDER BY KU_DATA_WAZ ASC ";
	
	private static final String SQL_GET_KUPON_LST_SALON = " select  kw.KR_OPIS AS OPIS,kw.KR_OPIS AS NAZWA,  kw.KR_ID AS ID , COALESCE(LI_URL,' ') as URL, "
			+ " kw.KR_USED as POBRANO, kw.KR_DATA_WAZ as  DATAW, ( kw.KR_LIMIT - kw.KR_USED ) AS ILE  from kuponw kw "
            + " left join link li on li.LI_US_ID = kw.KR_US_ID and li.LI_SA_ID = kw.KR_SA_ID and li.LI_ZR_ID = kw.KR_LI_ID "
            + " left join opish oh on oh.OH_US_ID = kw.KR_US_ID and oh.OH_SA_ID = kw.KR_SA_ID and oh.OH_ZR_ID = kw.KR_OH_ZR_ID "
            + " where kw.KR_US_ID = ? and kw.KR_SA_ID = ? AND ( ( KR_LIMIT - KR_USED ) > 0 )  ORDER BY KR_DATA_WAZ ASC ";
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public StdResponse getKuponRabatowy(int user, int salon, int card, int sysid, String password, int kuponZrId) {
		StdResponse sr = new StdResponse();
		String sql = "SELECT KU_ID from kupon where KU_US_ID = ? AND KU_SA_ID = ? and KU_KR_ZR_ID = ? AND KU_KA_ZR_ID = ?";
         List<Integer> id = jdbcTemplate.queryForList( sql, new Object[] { user, salon, kuponZrId, card }, Integer.class );
         if( id.size() > 0 )
         {
        	 System.out.println("KUPON ALLREADY EXISTS");
        	 sr.setDesc("KUPON ALLREADY EXISTS");
        	 sr.setId(id.indexOf(0));
        	 return sr;
         }
         else
         {
        	 System.out.println("KUPON NIE EXISTS"); 
         }

		return sr;
	}

	@Override
	public List<KuponUser> getKuponLstUserSalon(int user, int salon, int card) {
		RowMapper<KuponUser> rowMapper = new KuponUserRowMapper();	
		System.out.println("getKuponLstUserSalon");
		return this.jdbcTemplate.query(SQL_GET_KUPON_LST_SALON_USER, rowMapper, user, salon,card);
	}

	@Override
	public List<KuponUser> getKuponLstSalon(int user, int salon ) {
		RowMapper<KuponUser> rowMapper = new KuponUserRowMapper();	
		System.out.println("getKuponLstUserSalon");
		return this.jdbcTemplate.query(SQL_GET_KUPON_LST_SALON, rowMapper, user, salon );
	}

	@Override
	public int insert(KuponUser ku) {
		String sql = "INSERT INTO kupon (  KU_US_ID, KU_SA_ID, KU_KOD, KU_KA_ZR_ID , KU_KR_ZR_ID , KU_LIMIT,  KU_AKT ) VALUES (?,?,?,?,?,?,?)";
		int row = jdbcTemplate.update(sql,ku.getKuUsId(), ku.getKuSaId(), ku.getKuKod(), ku.getKuKaZrId(), ku.getKuKrZrId(), ku.getKuLimit(), ku.getKuAkt() );
		return row;
	}

}
