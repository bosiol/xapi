package pl.wojciechr.xapi.dao;


import java.util.List;
import java.util.Map;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.entity.mappers.CardKodExtractor;
import pl.wojciechr.xapi.entity.mappers.CardRowMapper;


@Repository
public class CardDAO implements ICardDAO {

	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Card> getAllCards() {
		String sql = "SELECT KA_ID, KA_KOD  FROM card";
        //RowMapper<Article> rowMapper = new BeanPropertyRowMapper<Article>(Article.class);
		RowMapper<Card> rowMapper = new CardRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	
	}

	@Override
	public Card getCardById(int Id) {
		String sql = "SELECT KA_ID, KA_KOD  FROM card WHERE KA_ID = ?";
		RowMapper<Card> rowMapper = new BeanPropertyRowMapper<Card>(Card.class);
		Card card = jdbcTemplate.queryForObject(sql, rowMapper, Id);
		return card;
		
	}

	@Override
	public String getKodById(int Id) {
		ResultSetExtractor rse = new CardKodExtractor();

		String sql = "SELECT KA_KOD  FROM card WHERE KA_ID= ?";
		return (String) jdbcTemplate.query(sql, rse, Id);
		
	}

	@Override
	public Card getByUserSalonSysId(int user, int salon, int sysid) {
		String sql = "SELECT KA_ID, KA_KOD, KA_AKT, KA_KWOTA_UP, KA_WLIMIT, KA_PUNKTY, KA_L1, KA_PUNKTYM, KA_PUNKTYMLM, KA_MINUTY FROM card ka join cardsal cs on cs.KAS_KA_ID = ka.KA_ID WHERE KA_US_ID = ? AND cs.KAS_SA_ID = ? AND KA_USER_SYS = ? LIMIT 1";
		RowMapper<Card> rowMapper = new CardRowMapper();
		Card card = jdbcTemplate.queryForObject(sql, rowMapper, user, salon, sysid);
		return card;
	}

}
