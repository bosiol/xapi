package pl.wojciechr.xapi.dao;

import java.util.List;

import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.StdResponse;

public interface IKuponDAO {
	StdResponse getKuponRabatowy(int user, int salon, int card, int sysid,  String password,  int kuponZrId);
	List<KuponUser> getKuponLstUserSalon(int user, int salon, int card);
	List<KuponUser> getKuponLstSalon(int user, int salon );
	int insert ( KuponUser ku);
}
