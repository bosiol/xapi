package pl.wojciechr.xapi.dao;

import java.util.List;

import pl.wojciechr.xapi.entity.Card;



public interface ICardDAO {
	List <Card> getAllCards();
	Card getCardById( int Id );
	String getKodById(int Id);
	Card getByUserSalonSysId(int user, int salon, int sysid);

}
