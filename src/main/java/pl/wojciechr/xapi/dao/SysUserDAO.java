package pl.wojciechr.xapi.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.entity.mappers.SysUserRowMapper;
@Repository
public class SysUserDAO implements ISysUserDAO {
	@Autowired
    private JdbcTemplate jdbcTemplate;
	@Override
	public SysUser getSysUser(String email, String password) {
		String sql = "SELECT * FROM sys_user WHERE  SU_EMAIL = ? AND SU_PASS = ? ";
		RowMapper<SysUser> rowMapper = new SysUserRowMapper();
		SysUser sysUser = jdbcTemplate.queryForObject(sql, rowMapper, email, password);
		return sysUser;
	}

	@Override
	public SysUser getSysUser(String email) {
		String sql = "SELECT * FROM sys_user WHERE   SU_EMAIL = ? ";
		RowMapper<SysUser> rowMapper = new SysUserRowMapper();
		SysUser sysUser = jdbcTemplate.queryForObject(sql, rowMapper, email);
		return sysUser;
	}

	@Override
	public int updateSysUser(SysUser sysUser) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertSysUser(SysUser sysUser) {
		String sql = " INSERT INTO sys_user (  SU_EMAIL, SU_PASS, SU_ANON, SU_FCMTOK, SU_TEL ) VALUES (?,?,?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(sql, sysUser.getSuEmail(), sysUser.getSuPass(), sysUser.getSuAnon(),
					sysUser.getSuFcmtok(), sysUser.getSuTel(), keyHolder );
		return keyHolder.getKey().intValue();
	}

	@Override
	public int deleteSysUser(SysUser sysUser) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SysUser getSysUser(int sysid, String password) {
		//String sql = "SELECT * FROM sys_user WHERE  SU_ID = ? AND SU_PASS = ? ";
		String sql = "SELECT * FROM sys_user WHERE  SU_ID = ?";
		RowMapper<SysUser> rowMapper = new SysUserRowMapper();
		try 
		{
			//SysUser sysUser = jdbcTemplate.queryForObject(sql, rowMapper, sysid, password);
			SysUser sysUser = jdbcTemplate.queryForObject(sql, rowMapper, sysid);
			return sysUser;
		}
		catch( EmptyResultDataAccessException e)
		{
			return new SysUser();
		}
		
	}

}
