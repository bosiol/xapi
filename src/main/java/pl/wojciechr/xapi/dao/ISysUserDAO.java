package pl.wojciechr.xapi.dao;

import pl.wojciechr.xapi.entity.SysUser;

public interface ISysUserDAO {
	
	SysUser getSysUser(String email, String password); 
	SysUser getSysUser(int sysid, String password); 
	SysUser getSysUser(String email); 
	int     updateSysUser( SysUser sysUser);
	int     insertSysUser( SysUser sysUser);
	int     deleteSysUser( SysUser sysUser);
}
