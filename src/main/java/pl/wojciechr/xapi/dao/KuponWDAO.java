package pl.wojciechr.xapi.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;



import pl.wojciechr.xapi.entity.Kuponw;
@Repository
public class KuponWDAO implements IKuponWDAO {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public Kuponw get(int user, int salon, int kuponZrId) {
		String sql = "SELECT * from kuponw where KR_US_ID = ? AND KR_SA_ID = ? and KR_ZR_ID = ?";

		RowMapper<Kuponw> rowMapper = new BeanPropertyRowMapper<Kuponw>(Kuponw.class);
		Kuponw  kw = jdbcTemplate.queryForObject(sql, rowMapper, user, salon, kuponZrId );
		return kw;
	}

	@Override
	public int updateUsed(Kuponw kw) {
		String sql = " UPDATE kuponw set KR_USED = ? WHERE KR_ID = ?";
		int row = jdbcTemplate.update(sql, kw.getKrUsed(), kw.getKrId());
		return row;
	}

}
