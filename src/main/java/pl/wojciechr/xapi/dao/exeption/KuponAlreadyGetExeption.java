package pl.wojciechr.xapi.dao.exeption;

public class KuponAlreadyGetExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public KuponAlreadyGetExeption(String message) {
		super(message);
	}
}
