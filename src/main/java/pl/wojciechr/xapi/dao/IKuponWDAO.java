package pl.wojciechr.xapi.dao;

import pl.wojciechr.xapi.entity.Kuponw;

public interface IKuponWDAO {
	Kuponw get(int user, int salon , int kuponZrId );
	int updateUsed( Kuponw kw );
}
