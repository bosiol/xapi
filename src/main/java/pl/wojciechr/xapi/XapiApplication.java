package pl.wojciechr.xapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class XapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(XapiApplication.class, args);
	}

}
