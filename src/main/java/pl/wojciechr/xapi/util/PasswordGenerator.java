package pl.wojciechr.xapi.util;

import java.security.SecureRandom;

public class PasswordGenerator {
	 private static SecureRandom random = new SecureRandom();

	    /** different dictionaries used */
	    private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	    private static final String NUMERIC = "0123456789";
	    private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

	    public static String generatePassword(int len, String dic) {
		String result = "";
		for (int i = 0; i < len; i++) {
		    int index = random.nextInt(dic.length());
		    result += dic.charAt(index);
		}
		return result;
	    }
	    
	    /*
	     * 
	    	int len = 10;
	System.out.println("Alphanumeric password, length " + len + " chars: ");
	String password = generatePassword(len, ALPHA_CAPS + ALPHA);
	System.out.println(password);
	System.out.println();

	len = 20;
	System.out.println("Alphanumeric + special password, length " + len + " chars: ");
	password = generatePassword(len, ALPHA_CAPS + ALPHA + SPECIAL_CHARS);
	System.out.println(password);
	System.out.println();

	len = 15;
	System.out.println("Alphanumeric + numeric + special password, length " + len + " chars: ");
	password = generatePassword(len, ALPHA_CAPS + ALPHA + SPECIAL_CHARS + NUMERIC);
	System.out.println(password);
	System.out.println();
	     * 
	     * 
	     */
	    
	    

}
