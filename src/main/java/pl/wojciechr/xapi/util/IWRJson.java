package pl.wojciechr.xapi.util;

import java.util.List;

public interface IWRJson<T> {
	 String convertToJson( List<T> lista);
	 String convertToJson( T obj);
	
}
