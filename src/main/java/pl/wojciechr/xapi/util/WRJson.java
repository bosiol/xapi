package pl.wojciechr.xapi.util;

import java.util.List;

public class WRJson<T> implements IWRJson<T> {

	@Override
	public String convertToJson(List<T> lista) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ \"output\" :[ ");
		int first = 0;
		for(T el : lista)
		{
			if(first > 0)
			{
				sb.append(',');
			}
			else
			{
				first = 1;
			}
			sb.append(convertToJson(el));
		}
		sb.append(" ] }");
		return sb.toString();
	}

	@Override
	public String convertToJson(T obj) {
		return obj.toString();
	}

}
