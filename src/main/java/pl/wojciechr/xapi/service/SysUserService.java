package pl.wojciechr.xapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wojciechr.xapi.dao.ISysUserDAO;
import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.service.interfaces.ISysUserService;
@Service
public class SysUserService implements ISysUserService {
	@Autowired
	private ISysUserDAO sysUSerDAO;

	@Override
	public StdResponse register(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StdResponse login(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StdResponse resetPassword(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StdResponse rememberPassword(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StdResponse update(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StdResponse delete(SysUser su) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SysUser get(String email, String password) {
		SysUser sysUser = sysUSerDAO.getSysUser(email, password);
		return sysUser;
	}

	@Override
	public SysUser get(int sysid, String password) {
		SysUser sysUser = sysUSerDAO.getSysUser(sysid, password);
		return sysUser;

	}
	
	
}
