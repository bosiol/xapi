package pl.wojciechr.xapi.service;

public class InvalidOrderItemException extends Exception {
	  public InvalidOrderItemException(String message) {
	      super(message);
	  }
	}