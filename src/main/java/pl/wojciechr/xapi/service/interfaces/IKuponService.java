package pl.wojciechr.xapi.service.interfaces;

import java.sql.SQLException;
import java.util.List;

import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.service.InvalidOrderItemException;

public interface IKuponService {

	
	List<KuponUser> getKuponLstUserSalon(int user, int salon, int card);
	List<KuponUser> getKuponLstUser( int user, int salon, int card);
	List<KuponUser> getKuponLstSalon( int user, int salon );
	StdResponse getKuponRabatowy(int user, int salon, int card, int sysid, int kupon , String password) throws InvalidOrderItemException, SQLException;
}
