package pl.wojciechr.xapi.service.interfaces;

import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.SysUser;

public interface ISysUserService {
	StdResponse register( SysUser su );
	StdResponse login( SysUser su );	
	StdResponse resetPassword( SysUser su );
	StdResponse rememberPassword( SysUser su );	
	StdResponse update( SysUser su );
	StdResponse delete( SysUser su );
	SysUser get( String email, String password);
	SysUser get( int sysid, String password);
	
}
