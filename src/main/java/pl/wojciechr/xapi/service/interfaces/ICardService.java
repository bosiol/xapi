package pl.wojciechr.xapi.service.interfaces;

import java.util.List;

import pl.wojciechr.xapi.entity.Card;



public interface ICardService {
	List <Card> getAllCards();
	Card getCardById( int Id );
	String getKodById(int id);
	Card getByUserSalonSysId(int user, int salon, int sysid);
}
