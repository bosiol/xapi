package pl.wojciechr.xapi.service;


import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import pl.wojciechr.xapi.dao.ICardDAO;
import pl.wojciechr.xapi.dao.IKuponDAO;
import pl.wojciechr.xapi.dao.IKuponWDAO;
import pl.wojciechr.xapi.entity.KuponUser;
import pl.wojciechr.xapi.entity.Kuponw;
import pl.wojciechr.xapi.entity.StdResponse;
import pl.wojciechr.xapi.entity.SysUser;
import pl.wojciechr.xapi.service.interfaces.IKuponService;
import pl.wojciechr.xapi.service.interfaces.ISysUserService;
@Service
public class KuponService implements IKuponService {
	
	
    private JdbcTemplate jdbcTemplate;
	@Autowired
	private IKuponDAO kuponDao;	
	
	@Autowired
	private IKuponWDAO kuponwDao;	
	
	@Autowired
	private ISysUserService sysUserService;


	private PlatformTransactionManager transactionManager;
	DefaultTransactionDefinition def;
	PlatformTransactionManager txManager;
	@Autowired
	public KuponService(JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager)
	{
		this.jdbcTemplate = jdbcTemplate;
		this.transactionManager = transactionManager;
		def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		txManager=new DataSourceTransactionManager(this.jdbcTemplate.getDataSource());
		
	}
	
	@Override
	public List<KuponUser> getKuponLstUserSalon(int user, int salon, int card) {
		System.out.println(" service getKuponLstUserSalon");
		return kuponDao.getKuponLstUserSalon(user, salon, card);
		
	}

	@Override
	public List<KuponUser> getKuponLstUser(int user, int salon, int card) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<KuponUser> getKuponLstSalon(int user, int salon) {
		System.out.println(" service getKuponLstSalon");
		return kuponDao.getKuponLstSalon(user, salon );
	}

	//@Transactional(rollbackFor = InvalidOrderItemException.class)
	@Override
	public StdResponse getKuponRabatowy(int user, int salon, int card, int sysid, int kupon , String password) throws InvalidOrderItemException, SQLException {
		StdResponse response = new StdResponse(0,0,"");
		TransactionStatus status = txManager.getTransaction(def);
		
		SysUser sysUser = sysUserService.get(sysid, password);
		
		if( sysUser == null || sysUser.getSuId() == 0 )
		{
			response.setId(0);
			response.setStatus(200);
			response.setDesc("Login error");
			System.out.println("eeror");
			return response;
		}
		response = kuponDao.getKuponRabatowy(user, salon, card, sysid, password, kupon);
		if( response.getId()  > 0 )
		{
			return response;
	
		}
		Kuponw kw = kuponwDao.get(user, salon, kupon);
		if( kw.getKrLimit()  <= kw.getKrUsed() && kw.getKrLimit() > 0  )
		{
			response.setId(0);
			response.setStatus(200);
			response.setDesc("brak kuponow");
			System.out.println("eeror");
			return response;
		}
		
		kw.setKrUsed(kw.getKrUsed()+1);
		kuponwDao.updateUsed(kw);
	//	
		//txManager.commit(status);
		txManager.rollback(status);
		try {
		KuponUser ku = new KuponUser();
		ku.setKuUsId(user);
		ku.setKuSaId(salon);
		ku.setKuKrZrId(kupon);
		ku.setKuKaZrId(card);
		ku.setKuLimit(1);
		ku.setKuAkt(1);
		ku.setKuKod(kw.getKrKod());
		int a = 0;
		int b = 1;
		
		if( a < b )
		{
			throw new InvalidOrderItemException( "Order quantity cannot be more than 100, found: " );
			
        }
				
		
		//int ret = kuponDao.insert(ku);
		}
		catch (InvalidOrderItemException e) {
	        //  logException(e);
			System.out.println("powinien byc rollback");
			//txManager.rollback(status);
			
	      }
		return response;
	}
}


