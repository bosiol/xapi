package pl.wojciechr.xapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.wojciechr.xapi.dao.ICardDAO;
import pl.wojciechr.xapi.entity.Card;
import pl.wojciechr.xapi.service.interfaces.ICardService;


@Service
public class CardService implements ICardService {
	@Autowired
	private ICardDAO cardDao;	
	@Override
	public List<Card> getAllCards() {
		return cardDao.getAllCards();
	}

	@Override
	public Card getCardById(int Id) {
		Card obj = cardDao.getCardById(Id);
		return obj;
	}

	@Override
	public String getKodById(int id) {
		
		return cardDao.getKodById(id);
	}

	@Override
	public Card getByUserSalonSysId(int user, int salon, int sysid) {
		return cardDao.getByUserSalonSysId(user, salon, sysid);
	}
	

}
